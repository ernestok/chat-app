const expect = require('expect')

const { isRealString, parsedQuery } = require('./validation')

describe('isRealString)', () => {
  it('should reject non-string query', () => {
    const res = isRealString(97)
    expect(res).toBeFalsy()
  })

  it('should reject string with only spaces', () => {
    const res = isRealString('   ')
    expect(res).toBeFalsy()
  })

  it('should allow string with non-space characters', () => {
    const res = isRealString('  space characters ')
    expect(res).toBeTruthy()
  })
})

describe('parsedQuery)', () => {
  it('should generate string query', () => {
    const param = {
      name: 'Habacki',
      room: 'Pokoj temu domowi'
    }
    const params = parsedQuery('name=Habacki&room=Pokoj+temu+domowi')
    expect(params).toMatchObject(param)
  })
})
