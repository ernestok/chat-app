const queryString = require('query-string')

const isRealString = (str) => {
  return typeof str === 'string' && str.trim().length > 0
}

const parsedQuery = (param) => {
  return queryString.parse(param)
}

module.exports = { isRealString, parsedQuery }
