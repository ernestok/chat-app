const expect = require('expect')

const { generateMessage, generateLocationMessage } = require('./message')

describe('genereateMessage)', () => {
  it('should generate correct message object', () => {
    const response = {
      from: 'Ad',
      text: 'message sent'
    }
    const message = generateMessage(response.from, response.text)
    Number.isInteger(message.createdAt)
    expect(message).toMatchObject({
      from: 'Ad',
      text: 'message sent'
    })
  })
})

describe('generateLocationMessage', () => {
  it('should generate correct location object', () => {
    const response = {
      from: 'Dev',
      latitude: 1,
      longitude: 2
    }
    const message = generateLocationMessage(response.from, response.latitude, response.longitude)
    Number.isInteger(message.createdAt)
    expect(message).toMatchObject({
      from: 'Dev',
      url: `https://mail.google.com/maps?q=1,2`
    })
  })
})
