const expect = require('expect')

const { Users } = require('./users')

describe('Users', () => {
  let someUsers

  beforeEach(() => {
    someUsers = new Users()
    someUsers.users = [{
      id: '1',
      name: 'Jacek',
      room: 'Wejcherowo'
    }, {
      id: '2',
      name: 'Kabacki',
      room: 'Kurnik'
    }, {
      id: '3',
      name: 'Lebacki',
      room: 'Wejcherowo'
    }]
    someUsers.noExistingUsersId = '12'
  })

  it('should add new user', () => {
    const someUsers = new Users()
    const user = {
      id: 'qwe123',
      name: 'Ibacki',
      room: 'Dolevas'
    }
    someUsers.addUser(user.id, user.name, user.room)

    expect(someUsers.users).toEqual([user])
  })

  it('should remove a user', () => {
    const userRemoved = someUsers.removeUser('2')

    expect(userRemoved.id).toEqual('2')
    expect(someUsers.users.length).toEqual(2)
  })

  it('should not remove user', () => {
    const userRemoved = someUsers.removeUser(someUsers.noExistingUsersId)

    expect(userRemoved).toBeFalsy()
    expect(someUsers.users.length).toEqual(3)
  })

  it('should find user', () => {
    const userGot = someUsers.getUser('2')

    expect(userGot.id).toEqual('2')
  })

  it('should not find user', () => {
    const userGot = someUsers.getUser(someUsers.noExistingUsersId)

    expect(userGot).toBeFalsy()
  })

  it('should return names for Wejcherowo room', () => {
    const userList = someUsers.getUserList('Wejcherowo')

    expect(userList).toEqual(['Jacek', 'Lebacki'])
  })

  it('should return names for Kurnik room', () => {
    const userList = someUsers.getUserList('Kurnik')

    expect(userList).toEqual(['Kabacki'])
  })
})
