var moment = require('moment')

// var date = moment()
// date.add(1, 'year').subtract(12, 'months')
// console.log(date.format('Do MMM, YYYY'))

var date = moment()
date.minute()
console.log(date.format('H:mm'))

var timestamp = moment.unix(moment(1541942026533).unix())
const currentTime = moment().valueOf()

console.log('timestamp: ', timestamp.format('YYYY, MMM d H:mm'))
console.log('currentTime: ', moment(currentTime).format('YYYY, MMM d H:mm'))

if (currentTime - timestamp > 3600000) {
  console.log('godzine temu')
  console.log('timestamp: ', timestamp.format('YYYY, MMM d H:mm'))
} else {
  console.log('mniej ni godzine temu')
  console.log(timestamp.format('H:mm'))
}
