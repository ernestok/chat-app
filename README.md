## Description

Chat application draw based on Andrew Mead's tutorials.

## Features: ##

    1. Possibility for creating chat rooms.
    2. Room depends on queries in url.
    3. One room for each window.
    4. Send your geolocation.
    5. Follow the latest messages in window room, if already scrolled to the bottom. 
    6. Show the time of message.
    7. Code tested.
    
    
## Author information ##

Ernest Kost

ernest.kost@gmail.com



## Installation and launching ##


### 1. Installing npm packages ###

  In order to launch the app, firstly you need to install the required npm packages.
  Change your directory to folder with cloned repo then use the command:
  
  `npm install`

  
### 2. Launching the app ###

  In order to see website on local machine go to main folder (where the package.json is) and use command:
  
  `npm run start`
  
  and open `http://localhost:3001/` in your browser
  
  
###  Used tools and frameworks ###

  
  - [Express](https://expressjs.com)
  - [Socket.io](https://socket.io)
  - [Webpack](https://webpack.js.org)
  - [Mocha](https://mochajs.org/)
  - [Moment.js](https://momentjs.com)
