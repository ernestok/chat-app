const socket = io() // eslint-disable-line

function scrollToBottom () {
  const messages = document.getElementById('messages')
  const newMessage = messages.lastElementChild

  const clientHeight = messages.clientHeight
  const scrollTop = messages.scrollTop
  const scrollHeight = messages.scrollHeight
  const newMessageHeight = newMessage.clientHeight
  const lastMessageHeight = newMessage.previousElementSibling ? newMessage.previousElementSibling.clientHeight : null

  if (clientHeight + scrollTop + newMessageHeight + lastMessageHeight >= scrollHeight) {
    messages.scrollTop = scrollHeight
  }
}

socket.on('connect', function () {
  const params = document.location.search

  socket.emit('join', params, function (err) {
    if (err) {
      alert(err)
      window.location.href = '/'
    } else {
      console.log('No error')
    }
  })
})

socket.on('disconnect', function () {
  console.log('Disconnected from server')
})

socket.on('updateUserList', function (users) {
  const ol = document.createElement('ol')
  const container = document.getElementById('users')

  while (container.firstChild) {
    container.removeChild(container.firstChild)
  }

  users.forEach(function (user) {
    const li = document.createElement('li')
    li.textContent = user

    ol.appendChild(li)
  })
  container.appendChild(ol)
})

const messageForm = document.getElementById('message-form')
messageForm.addEventListener('submit', function (e) {
  e.preventDefault()
  const messageInput = document.querySelector('input[name=message]')
  emitMessage(messageInput.value)

  function emitMessage (message) {
    socket.emit('createMessage', {
      // from: 'User',
      text: message
    }, function () {
      messageInput.value = ''
    })
  }
})

function createTemplate (string, attributes) {
  const template = document.getElementById(string).textContent
  const html = Mustache.render(template, attributes)
  const ol = document.getElementById('messages')
  ol.insertAdjacentHTML('beforeend', html)
}

socket.on('newMessage', function (message) {
  const formatedTime = moment(message.createdAt).format('H:mm')
  createTemplate('message-template', {
    text: message.text,
    from: message.from,
    createdAt: formatedTime
  })
  scrollToBottom()
})

socket.on('newLocationMessage', function (message) {
  const formatedTime = moment(message.createdAt).format('H:mm')
  createTemplate('location-message-template', {
    href: message.url,
    from: message.from,
    createdAt: formatedTime
  })
  scrollToBottom()
})

const locationButton = document.getElementById('send-location')
locationButton.addEventListener('click', function () {
  if (!navigator.geolocation) {
    return alert('Geolocation not supported by your browser.')
  }

  function ButtonMessage (make) {
    if (make === 'reset') {
      locationButton.removeAttribute('disabled')
      locationButton.textContent = 'Send location'
    } else {
      locationButton.setAttribute('disabled', 'disabled')
      locationButton.textContent = 'Sending..'
    }
  }

  ButtonMessage()

  navigator.geolocation.getCurrentPosition(function (position) {
    ButtonMessage('reset')
    socket.emit('createLocationMessage', {
      latitude: position.coords.latitude,
      longitude: position.coords.longitude
    })
  }, function () {
    ButtonMessage('reset')
    alert('Unable to fetch location')
  })
})
